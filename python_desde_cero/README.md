# Python desde cero

Charla sobre como empezar a programar en python si no sabes nada

El link a la presentación es [https://gitlab.com/HoracioNakama/python_desde_cero](https://gitlab.com/HoracioNakama/python_desde_cero)

## Qué es Python?

Es un lenguaje de programación. Aprovecho para rescatar dos características: es relativamente sencillo de aprender (Curva de Aprendizaje) y muy legible. 

Pueden aprender más en la [página de Wikipedia](https://es.wikipedia.org/wiki/Python).

## Quién escribe esto?

Horacio Mayo (@HoracioNakama) Puedes encontrarme en Twitter, Instagram, Telegram, y otras redes sociales menos visitadas por mí. Estudié Agronomía, aprendí a programar con Python en el 2016. Soy un científico, uso python en mis proyectos de investigación (Agroclimatología principalmente). Sagitario. Vivo en Tilcara, Prov. de Jujuy, Argentina. 

## Aclaración inicial

Considero que en internet hay mucho material para empezar en la programación con Python, no creo que sea necesario generar un nuevo material por lo que los siguiente serán atajos al conocimiento (en forma de links o tips)

Traducción: si digo "librería" imaginatelo como una herramienta. Si quiero atornillar algo uso un destornillador. Si quiero trabajar con números muy grandes en python utilizo la librería Numpy

## Resumen

Mi recomendación son simples pasos

1. Adquiere el conocimiento
2. Usa el conocimiento
3. Reevalúa tu conocimiento

## Si no sabes nada todavia Por dónde empezar? (Adquirir el conocimiento)

En los siguientes links encontrarán bibliografía en español e inglés, links a charlas, etc. para aprender lo básico de Python.

[Quiero aprender Python (Argentina en Python)](https://argentinaenpython.com/quiero-aprender-python/)

[Aprendiendo Python (PyAr)](http://www.python.org.ar/wiki/AprendiendoPython)

Y hay videos? Obviamente sí, aquí hay dos que he escuchado mucho que recomiendan:

[Curso de Python (Código Facilito)](https://www.youtube.com/playlist?list=PLE549A038CF82905F)

[Curso Python desde 0 (Pildoras Informáticas)](https://www.youtube.com/playlist?list=PLU8oAlHdN5BlvPxziopYZRd55pdqFwkeS)

Qué es mejor? Libro o Video? Depende. Yo te recomendaría leer o ver el video un tiempo (corto) y si entiendes el contenido, sigas con esa opción.

Y si quieres ganar tiempo, entra a un curso virtual o presencial. Es mejor cuando tienes un profe a tu lado que te orienta y puedes consultarle.

Tips: Si quieres saber que estás leyendo algo actualizado fijate cuando usan la función print que tenga parentesis. Así print(), eso significa que están usando la versión Python 3.

## Ok ya hemos consultado el material... ahora las manos a la masa!! (Usa el conocimiento)

Instala Python en tu sistema operativo (Te recomiendo consultar el anterior paso donde seguro enseñan como instalarlo)

Tips: la mejor forma de aprender algo (para mí) es relacionarlo con algo que te gusta. Entonces piensa que problema quieres resolver con programación y empieza a caminar en esa dirección.

¿Cómo darte cuenta de eso? Simple, fijate que tarea repetitiva haces en la computadora. Ahí creo que hay un problema que podrías buscarle solución.

Abajo he listado los proyectos más comunes en quienes inician en Python. Asi qué piensa que quieres y seguro hay una librería para vos.

Quiero...

Analizar datos y usar estadística básica: empieza con <u>Pandas</u> y <u>Seaborn</u>. (Si vienes de usar Excel, esto es lo más cercano en Python)

Hacer análisis contable, usa las librerías anteriores (al menos al principio)

Bajar datos de internet, aprende lo que es <u>Scrapear</u> y <u>Web Crawler</u> (las librerías mas usadas son Requests, <u>BeautifulSoup</u> y <u>Scrapy</u>)

Hacer juegos: Python quizás no es el lenguaje más recomendado, pero hay forma. Prueba con <u>Pilas engine</u>.

Hacer una aplicación para mi celular: hay formas mejor, pero se puede. Investiga <u>Kivy</u>.

Armar mi página web: Si es un blog (algo estatico ) recomendaría <u>Nikola</u> o <u>Pelican</u>. Y si requiere más interactividad (formularios, envio de emails, inteligencia artificial, etc) <u>Django</u>.

Trabajar con base de datos: utiliza ORM como Peewee o sqlalchemy.

Todo lo anteriormente indicado requiere otros conocimientos anexos (en base de datos SQL, en páginas web HTML y CSS, etc.) pero mencioné las librerías más utilizadas en python.

Y si no es ninguno de esos proyectos puedes ir a [Pypi](https://pypi.org/), que hay varias librerías de Python que podrían servirte.

## Ya sé lo básico y realicé mi primer proyecto (Reevalúa tu conocimiento)

El primer proyecto siempre sale feo, es inevitable y natural (como cuando subimos por primera vez a la bici y terminamos con heridas). Pero si seguimos practicando y aprendiendo nuevas cosas, estaremos más cerca de mejorar como programadores. Aquí van algunos consejos.

- Alejate (pero no demasiado) de [Stackoverflow](https://stackoverflow.com/). Para quién no lo conoce este sitio web tiene varias consultas y sus respuestas a problemas o dudas en la programación. Está bien consultar algunas veces, pero no es recomendable abusar del copiar y pegar código. Por eso no te alejes demasiado, porque a veces ahí encuentras muchas soluciones
- Consulta otras fuentes: Puedes preguntar en el grupo de Telegram de Python Argentina @pythonargentina o de Python España @PythonEsp. En el canal IRC #pyar, o en la [lista de correo de PyAr](http://www.python.org.ar/lista/).
- Sube un nivel: Puedes leer el libro online [Python Intermediate](https://book.pythontips.com/en/latest/index.html). Te permitirá mejorar tus habilidades de programador python, tiene ejemplos sencillos y temas bien explicado (La contra: está en ingles)
- Hay newsletter semanales que te envian por email links a tutoriales, videos, noticias, ofertas de trabajo y más, relacionado con Python (de nuevo la contra que están en inglés). Está [Awesome Python](https://python.libhunt.com/newsletter?f=es-top-d), la [Python Weekly](https://www.pythonweekly.com/) y [Pycoder's Weekly](https://pycoders.com/)
- Herramientas: Pueden usar Ipython, Bpython, Ptpython que le ponen esteroides al Interprete Interactivo de Python. [Les dejo un video](https://youtu.be/aP_P95eOpis) donde muestran las mencionadas herramientas. También está [Visual Studio Code](https://code.visualstudio.com/), que es un IDE que está ganando fuerza para programar en Python porque tiene varias funciones útiles. Les dejo un [video de Youtube](https://youtu.be/6YLMWU-5H9o) donde muestran varias funciones interesantes (English again!)
- Git: este sistema de control de versiones permite poder avanzar como programador, muy útil para poder restaurar versiones de nuestro código si cometemos un error y compartirla entre otros programadores y la comunidad. Los sistemas más "famosos" son [Github](https://github.com/) y [Gitlab](https://about.gitlab.com/).
- Charlas: Al tener una gran comunidad pueden encontrarse muchas charlas relacionadas en Python. En Argentina la más grande es la [PyConAr (Youtube)](https://www.youtube.com/channel/UCjYLIv07fw21w0uIAtUMnNA), también hay canales como el [Canal de Youtube Python España](https://www.youtube.com/channel/UCyth_6hqft9a7B_thdwYyww) y a nivel Científico las más conocida es [PyData](https://www.youtube.com/user/PyDataTV)

## Ultimas palabras

Sea cual sea tu inicio o tu origen, trata de juntarte con alguien. Python tiene una comunidad cálida y acogedora (e hispanohablante) que tiene pocos problemas para ayudarte. Además, de que cualquier crecimiento (personal o grupal) es más rápido y gratificante en grupo.

Aprende a vivir con el error. Meter la pata es natural y sano. En algún momento deberás mostrar tu código a alguien, y te dirán (espero que de forma amable) si cometiste un error o como mejorarlo. Y así uno aprende, por eso recomiendo nuevamente el pasó dos, escribe código porque a cada paso serás un mejor programador.

Probablemente, de lo escrito hayas entendido menos de la mitad (15% si soy optimista), pero está bien. Esto es nuevo. Prueba algo de la bibliografía del primer paso y pasado un tiempo vuelve aquí, seguro que entenderás un poco más. Y así, repetidas veces.

Saludos!