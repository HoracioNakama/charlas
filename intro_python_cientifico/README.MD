# Charla Introducción a Python Científico: Jupyter Notebook y Pandas
Charla dictada en Salta, en INENCO-UNSa el 17 de agosto de 2017.

Fuente: [Página oficial Jupyter](http://jupyter.org/)

## Prerequisitos

Mientras Jupyter corre codigo en varios lenguajes de programación. Python es requerido (Python 3.3 o superior, o Python 2.7) para instalar Jupyter Notebook.

## Instalando Jupyter y Pandas usando Anaconda

El autor del repositorio recomienda instalar Python y Jupyter usando la Distribución Anaconda, la cual incluye Python, Jupyter Notebook, Pandas y otros paquetes (Scipy, Matplotlib, Numpy, etc.) comunmente usados para computación científica y ciencia de Datos. Y esto resultara muy ventajoso si inicia en la programación y tiene pocos conocimientos, permitiendo más adelante migrar a otras herramientas.

Primero, descargar [Anaconda](https://www.anaconda.com/downloads). Se recomienda descargar la ultima versión Python 3 de Anaconda.

Segundo, instalar al versión de Anaconda que usted descargo, siguiendo las intrucciones de la pagina de descarga para el sistema operativo que usted utilice.

Tras finalizada la instalación, para correr el Notebook, corra el siguiente comando en la Terminal (Mac/Linux) o en el CMD (Windows):

```bash
jupyter notebook
```

## Instalando Jupyter y Pandas con pip

Otra forma, recomendada para usuarios más experimentados de Python, es utilizar  el gestor de Paquetes de Python: pip.

SI tiene Python 3 instalado (que es lo recomendado):

```bash
python3 -m  pip install --upgrado pip
python3 -m  pip install jupyter
python3 pip install pandas
```

Si tiene Python 2 instalado:

```bash
python -m pip install --upgrado pip
python -m pip install jupyter
```

Para correr el notebook, corre el siguiente comando en la Terminal (Mac/Linux) or CMD (Windows)

```bash
jupyter notebook
```

